# Bramble

**Bramble** is an api-first task management tool,
backed by postgres
and a micro-service architecture.
A successor to
[next-task](),
this is an entirely re-designed application.

## Design

This application is designed to be hosted on a kubernetes cluster.
Broadly, this app has been designed with these principles:

- Python run times preferred
- Endpoints are supplied with well-scoped least privilege permissions
- Well structured api.

For more design documentation check out the [design docs](./docs/design.md)

## Installation

TBD - Helm charts

## Developer Notes

This repository is a mono-repo.
All sub-projects are managed independently.
Pedantic development choices.

- `poetry` is the python package manager of choice.
- `Containerfiles` is the preferred term for build files.
- Conventional commits MUST be used, [reference list](#conventional-commits)

### Versioning

All packages are tracked as `MAJOR`.`MINOR`.`PATCH`

### Conventional Commits

```
INIT # structural changes to pakage contents
FUNC # functional changes
DOCS # documentation
TEST # commits adding tests to the repository
LINT # corrections to formatting or spelling
REFACTOR # Non functional changes to functions improving performance or readability
```
