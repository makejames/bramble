# Contributing to Next Task

Thanks for your input!
This guide is here to make contributing to the project as easy as possible.
If you have any questions feel free to reach out to a maintainer.

## About the project

This project is hosted on [gitlab][repo]
and uses gitlab issues to track bugs,
support questions and feature requests.

When raising a feature request feel free to check out the
[design documentation](./docs/design.md)
to see if the feature has already been identified
or if it is compatible with the short or long term vision of the project.

## The Code

All code in this project falls under the [MIT licence](./LICENCE).
It is assumed that any code submitted for review is under the same LICENCE.
Please contact a maintainer if this is a concern.

## Development

These guidelines are meant to improve communication
and make it easier to understand the scope and changes in the code.

Merge requests failing these guidelines may be rejected.

### Branching

New functionality should branch from `main`
with a Merge request raised for review.
Branches must use a conventional naming structure such as:

```sh
feature/feature-name
docs/document-name
fix/issue-summary
chore/update-library
```

Linting changes, unrelated to the functional change,
should be made on separate branch.

### Commit Messages

Conventional commit guidelines are strictly necessary.
Commits should be small and specific and use one of the following prefixes.
Where a commit bridges more than one of these categories
consider splitting the commit or choose the most appropriate.

```sh
FUNC: # Functional change
FIX: # Fix to an issue
INIT: # Structural change relating to the package or its packaging
TEST: # Addition of tests
LINT: # Correction to formatting or spelling
DOCS: # Documentation changes
REFACTOR: # Refactoring changes with no functional difference
```

### Structure

This project uses a mono repo of sub-projects.
Whilst some directories may not need all of these,
it is expected that most sub-projects will contain a:

- `README.md`
- `Makefile`
- `.gitlab-ci.yml`
- `Containerfile`
- `pyproject.toml`

### Pipelines

This repository is hosted on GitLab
and uses GitLab's CI/CD features.
Steps should be taken to ensure that CI/CD run times are performant.
To help manage sub-project pipelines it is recommended to review
[the documentation on parent-child pipelines](https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html#parent-child-pipelines).

### Tests

Functional changes should be accompanied by tests.
This project uses the **R-BICEP** annotation of unit tests:

- **R** – are the results right?
- **B** – are all the boundary conditions CORRECT?
- **I** – can you check inverse relationships?
- **C** – can you cross-check results using other means?
- **E** – can you force error conditions to happen?
- **P** – are performance characteristics within bounds?

With Boundary Conditions measured against **CORRECT**

- **Conform** – Does the value conform to an expected format?
- **Order** – is the set of values ordered or unordered as appropriate?
- **Range** – is the value within reasonable minimum and maximum values?
- **Reference** – does the code reference anything external
- **Existence** – Does the value exist?
- **Cardinality** – are there exactly enough values?
- **Time** – is everything happening in order? At the right time? In time?

When submitting an MR to fix to a defect,
there must be sufficient tests to prove that the fix has had an effect.

### Linting

Existing python subprojects use a range of pedantic linting tools including:

- [`pyflakes`](https://github.com/PyCQA/pyflakes)
- [`pycodestyle`](https://pycodestyle.pycqa.org/en/latest/)
- [`pydocstyle`](http://www.pydocstyle.org/en/stable/)
- [`mypy`](https://mypy.readthedocs.io/en/stable/)

Subprojects with **SQL** embedded in them should use
[`SQLFluff`](https://docs.sqlfluff.com/en/stable/)

**Markdown** files should follow [SEM-BR](https://sembr.org/).

### Pre-merge checklist
- Unit tests are written and coverage is at 85%
- README and extended Documentation is up to date
- CHANGELOG for tag is completed

### Semantic Versioning

This Project uses semantic versioning `Major.Minor.Patch`.
Major implies some break in backwards compatibility.

The package itself tracks version changes to the api,
the sub-projects track changes to the sub-project

Version increments before 1.0.0 imply an api is still open to change
and dependency approached with caution.

[repo]: https://gitlab.com/makejames/bramble/
