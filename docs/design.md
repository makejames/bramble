# Design

**Bramble** is an api-first task management solution.

Designed on the principle of getting thing done,
Bramble streamlines the decision making out of task list management.

At any time a user can create a new task,
to be added to the prioritised task list.
A user can only interact with the next priority task.
They can either skip or close that task.

The older a task is,
the more often that task will appear as the next priority task.

With task management, simplicity is key.
Additional cognitives load in sorting
or categorising tasks delays getting tasks done.

Built to run in a kubernetes cluster,
this micro-service application is designed to be performant
and extensible.

```mermaid
flowchart LR
    c(task-db-create)
    db[(task-db)]
    a(auth)
    c --> db
    db --> a

    w(task-api.write)
    r(task-api.read)
    s(task-api.static)

    w --> db --> r
```

## V1.0 Requirements

- MUST be able to create a task
- Tasks MUST be prioritised at rest
(the next task remains the same until user input)
- MUST be able to request the next task
- MUST be able to close/complete a task
- MUST be able to skip a task
- MUST be able to request a task list
- COULD be able to request and view an individual task

## Future design considerations

Here is a collection of thoughts that are out of scope for initial app.

### Add notes to tasks

Either as part of skipping or as part of completion.

**Requirements**

- MUST be able to add notes to a task.
- MUST be able to view notes of the current task.
- SHOULD be able to add additional notes to a task with notes.
- SHOULD be able to view notes of a given task.
- COULD be able to edit existing notes.

### Group tasks with labels

Labels, projects or other means of grouping tasks adds additional complication.
Given that is the case,
there is utility in giving a user the ability to group tasks.

**Requirements**

- MUST be able to create tasks in group (project, label or both)
- MUST be able to request the next task from a group
- Groups MUST be unique to each USER
- SHOULD be able to list tasks based on a group
- COULD be able to add or change a group to an existing task

### CLI

A cli to interface with the api in a terminal.

### GUI

Could there be a GUI htmx web app?
